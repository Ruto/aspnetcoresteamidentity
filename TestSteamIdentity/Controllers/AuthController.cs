﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;
using TestSteamIdentity.Services;

namespace TestSteamIdentity.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : Controller
    {
        private readonly IUserService _userService;

        public AuthController(IUserService authService)
        {
            _userService = authService;
        }

        // do steam login, go to next endpoint /api/auth/done
        [HttpGet("login")]
        public IActionResult Signin()
        {
            return Challenge(new AuthenticationProperties { RedirectUri = "/api/auth/done" }, "Steam");
        }

        // send token, use token in authorizzation: bearer in postman or insomnia or something
        [HttpGet("done")]
        public async Task<IActionResult> Done()
        {
            var result = await HttpContext.AuthenticateAsync("Steam");

            long steamid64 = ParseSteamId64(result.Principal.FindFirst(ClaimTypes.NameIdentifier).Value);

            var user = await _userService.Authenticate(steamid64, result.Principal.FindFirst(ClaimTypes.Name)?.Value);

            return Ok(user);
        }

        // try to access this with bearer token
        [Authorize]
        [HttpGet]
        public IActionResult GetAll()
        {
            return Json(_userService.GetAll());
        }

        // parse steamid64 from openid url
        private long ParseSteamId64(string url)
        {
            return long.Parse(url.Substring(url.LastIndexOf('/') + 1));
        }
    }
}