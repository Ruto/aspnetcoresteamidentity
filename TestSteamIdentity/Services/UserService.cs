﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TestSteamIdentity.Model;

namespace TestSteamIdentity.Services
{
    public interface IUserService
    {
        Task<SteamUser> Authenticate(long steamid64, string name = null);

        IEnumerable<SteamUser> GetAll();
    }

    public class UserService : IUserService
    {
        public static byte[] _secret = System.Text.Encoding.ASCII.GetBytes("this should be somewhere more secure but we testing");

        private DataContext _ctx;

        public UserService(DataContext ctx)
        {
            _ctx = ctx;
        }

        public IEnumerable<SteamUser> GetAll()
        {
            return _ctx.Users;
        }

        public async Task<SteamUser> Authenticate(long steamid64, string name = null)
        {
            var user = _ctx.Users.SingleOrDefault(x => x.SteamID64 == steamid64);

            if (user == null)
            {
                user = new SteamUser()
                {
                    SteamID64 = steamid64,
                    Name = name
                };
                _ctx.Users.Add(user);
            }

            // do i check if token already exists?
            user.Token = GenerateToken(user);
            await _ctx.SaveChangesAsync();

            return user;
        }

        private string GenerateToken(SteamUser user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("steamid64", user.SteamID64.ToString(), ClaimValueTypes.UInteger64),
                    new Claim(ClaimTypes.Role, "Admin")
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(_secret), SecurityAlgorithms.HmacSha512Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}