﻿using Microsoft.EntityFrameworkCore;

namespace TestSteamIdentity
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Model.SteamUser> Users { get; set; }
    }
}