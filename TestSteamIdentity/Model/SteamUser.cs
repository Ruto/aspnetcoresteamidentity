﻿namespace TestSteamIdentity.Model
{
    public class SteamUser
    {
        public int ID { get; set; }

        public long SteamID64 { get; set; }

        public string Name { get; set; }

        public string Token { get; set; }
    }
}